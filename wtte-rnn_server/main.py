import numpy as np
import random
import tensorflow as tf
import time

import warnings
warnings.filterwarnings('ignore')

""" init base methods """


def weibull_cdf(t, a, b):
    t = np.double(t) + 1e-35
    return 1 - np.exp(-np.power(t / a, b))


def weibull_hazard(t, a, b):
    t = np.double(t) + 1e-35
    return (b / a) * np.power(t / a, b - 1)


def weibull_pdf(t, a, b):
    t = np.double(t) + 1e-35
    return (b / a) * np.power(t / a, b - 1) * np.exp(-np.power(t / a, b))


def weibull_cmf(t, a, b):
    t = np.double(t) + 1e-35
    return weibull_cdf(t + 1, a, b)


def weibull_pmf(t, a, b):
    t = np.double(t) + 1e-35
    return weibull_cdf(t + 1.0, a, b) - weibull_cdf(t, a, b)


def weibull_mode(a, b):
    mode = a * np.power((b - 1) / b, 1 / b)
    mode[b <= 1] = 0
    return mode


def weibull_mean(a, b):
    import scipy.special.gamma as gamma
    return a * gamma(1 + 1 / b)


def weibull_quantiles(a, b, p):
    return a * np.power(-np.log(1 - p), 1 / b)


def weibull_continuous_logLik(t, a, b, u=1):
    return u * np.log(weibull_pdf(t, a, b)) + (1 - u) * np.log(1 - weibull_cdf(t, a, b))


def weibull_discrete_logLik(t, a, b, u=1):
    return u * np.log(weibull_pmf(t, a, b)) + (1 - u) * np.log(1 - weibull_cdf(t + 1, a, b))


def np_bernoulli_logLik(p, v):
    return (v * np.log(p) + (1 - v) * np.log(1 - p))


def weibull_logLik_continous(a_, b_, y_, u_, name=None):
    ya = tf.div(y_ + 1e-35, a_)
    return (
        tf.mul(u_,
               tf.log(b_) + tf.mul(b_, tf.log(ya))
               ) -
        tf.pow(ya, b_)
    )


def weibull_logLik_discrete(a_, b_, y_, u_, name=None):
    with tf.name_scope(name):
        hazard0 = tf.pow(tf.div(y_ + 1e-35, a_), b_)
        hazard1 = tf.pow(tf.div(y_ + 1, a_), b_)
    return (tf.mul(u_, tf.log(tf.exp(hazard1 - hazard0) - 1.0)) - hazard1)


def weibull_beta_penalty(b_, location=10.0, growth=20.0, name=None):
    with tf.name_scope(name):
        scale = growth / location
        penalty_ = tf.exp(scale * (b_ - location))
    return (penalty_)


def create_layer(input_, n_input, n_output, name=None, activation_function=tf.nn.softplus, init_biases=None):
    with tf.name_scope(name):
        with tf.name_scope("weights"):
            weights = tf.Variable(
                tf.truncated_normal(
                    [n_input, n_output],
                    stddev=1.0 / np.sqrt(float(n_input))))
        with tf.name_scope("biases"):
            if init_biases is None:
                biases = tf.Variable(tf.zeros([n_output]))
            else:
                biases = tf.Variable(init_biases)
        with tf.name_scope('Wx_plus_b'):
            activations = tf.matmul(input_, weights) + biases
        with tf.name_scope('output'):
            output = activation_function(activations)
    return output


def roll_fun(x, size, fun=np.mean):
    y = np.copy(x)
    n = len(x)
    size = min(size, n)

    if size <= 1:
        return x

    for i in range(size):
        y[i] = fun(x[0:(i + 1)])
    for i in range(size, n):
        y[i] = fun(x[(i - size + 1):(i + 1)])
    return y


def na_locf(x):
    v = np.isnan(x)
    real_val = 0
    for i in range(len(x)):
        if v[i]:
            x[i] = x[real_val]
        else:
            real_val = i
    return x

print("base methods initialized")

""" init models """
# Creating RNN for prices deltas sign
print("first model initializing...")

max_seq_len = 20
n_features = 1
state_size = 4
n_output = 2

init_alpha_ = tf.placeholder_with_default(tf.ones([1]), [1], name='init_op_layer')
init_beta_ = tf.placeholder_with_default(tf.ones([1]), [1], name='init_op_layer')
penalty_parameter_ = tf.placeholder(tf.float32, name='penalty_parameter')
lr_ = tf.placeholder(tf.float32, name='learning_rate')
batch_size_ = tf.placeholder(tf.int32, name='batch_size')

X_ = tf.placeholder("float32", shape=[max_seq_len, None, n_features], name='features')
Y_ = tf.placeholder("float32", shape=[max_seq_len, None], name='waiting_times')
U_ = tf.placeholder("float32", shape=[max_seq_len, None], name='censoring_indicators')

early_stop_ = tf.placeholder(tf.int32, name='early_stop_w')


def simple_lstm(X_, early_stop, batch_size_, name=None):
    with tf.name_scope(name):
        with tf.name_scope(name + "/input_layer"):
            X_flat = tf.reshape(X_, [-1, n_features])

            hidden1 = create_layer(X_flat, n_features,
                                   state_size,
                                   name=name + '/hidden1',
                                   activation_function=tf.nn.tanh)
            hidden1_out = tf.split(0, max_seq_len, hidden1)

        cell = tf.nn.rnn_cell.BasicLSTMCell(state_size, forget_bias=1.0)
        initial_state = cell.zero_state(batch_size_, tf.float32)
        rnn_outputs, states = tf.nn.rnn(cell,
                                        hidden1_out,
                                        initial_state=initial_state,
                                        scope=name + '/RNN',
                                        sequence_length=early_stop)

        with tf.name_scope("output_layer%s" % (name)):
            rnn_outputs_nice = tf.pack(rnn_outputs)
            rnn_outputs_flat = tf.reshape(rnn_outputs_nice, [-1, state_size])

            init_op_layer = tf.concat(0, [
                tf.log(init_alpha_),  # centers exp      at init_alpha
                tf.log(tf.exp(init_beta_) - 1.0)  # Centers softplus at init_beta
            ])
            op_activation = create_layer(rnn_outputs_flat,
                                         state_size,
                                         n_output, name='output_layer',
                                         activation_function=tf.identity,
                                         init_biases=init_op_layer
                                         )
    return op_activation


def training_step_rnn(logLik_raw, early_stop_, lr_):
    with tf.name_scope("training"):
        cost = -tf.reduce_mean(logLik_raw[:early_stop_, :])
        step = tf.train.RMSPropOptimizer(learning_rate=lr_, epsilon=1e-5).minimize(cost)
    return cost, step


with tf.name_scope("weibull_lstm"):
    op_activation = simple_lstm(X_, early_stop_, batch_size_, name="RNN_weibull")
    op_activation = tf.reshape(op_activation, [max_seq_len, batch_size_, 2])

    a_, b_ = tf.split(split_dim=2, num_split=2, value=op_activation)

    a_ = tf.exp(a_[:, :, 0])
    b_ = tf.nn.softplus(b_[:, :, 0])

    output_layer_w = tf.pack([a_, b_], axis=2)
    log_likelihood = weibull_logLik_discrete(a_, b_, Y_, U_, name="logLikelihood")

    cost_w, step_w = training_step_rnn(log_likelihood, early_stop_, lr_)

    log_likelihood_regularized = log_likelihood - weibull_beta_penalty(b_, location=8.0, growth=10.0,
                                                                       name='penalty_calculation')
    cost_w_pen, pen_step_w = training_step_rnn(log_likelihood_regularized, early_stop_, lr_)

sess = tf.InteractiveSession()
saver = tf.train.Saver()

print("first model initialized")
# ---------------------------------------

# Creating RNN for amplitude > commission event predicting

print("second model initializing...")

max_seq_len1 = 19
n_features1 = 1
state_size1 = 4
n_output1 = 2

init_alpha_1 = tf.placeholder_with_default(tf.ones([1]), [1], name='init_op_layer1')
init_beta_1 = tf.placeholder_with_default(tf.ones([1]), [1], name='init_op_layer1')
penalty_parameter_1 = tf.placeholder(tf.float32, name='penalty_parameter1')
lr_1 = tf.placeholder(tf.float32, name='learning_rate1')
batch_size_1 = tf.placeholder(tf.int32, name='batch_size1')

X_1 = tf.placeholder("float32", shape=[max_seq_len1, None, n_features1], name='features1')
Y_1 = tf.placeholder("float32", shape=[max_seq_len1, None], name='waiting_times1')
U_1 = tf.placeholder("float32", shape=[max_seq_len1, None], name='censoring_indicators1')

early_stop_1 = tf.placeholder(tf.int32, name='early_stop_w1')


def simple_lstm1(X_1, early_stop1, batch_size_1, name=None):
    with tf.name_scope(name):
        with tf.name_scope(name + "/input_layer1"):
            X_flat1 = tf.reshape(X_1, [-1, n_features1])

            hidden11 = create_layer(X_flat1, n_features1,
                                    state_size1,
                                    name=name + '/hidden11',
                                    activation_function=tf.nn.tanh)
            hidden1_out1 = tf.split(0, max_seq_len1, hidden11)

        cell1 = tf.nn.rnn_cell.BasicLSTMCell(state_size1, forget_bias=1.0)
        initial_state1 = cell1.zero_state(batch_size_1, tf.float32)
        rnn_outputs1, states1 = tf.nn.rnn(cell1,
                                          hidden1_out1,
                                          initial_state=initial_state1,
                                          scope=name + '/RNN1',
                                          sequence_length=early_stop1)

        with tf.name_scope("output_layer1%s" % (name)):
            rnn_outputs_nice1 = tf.pack(rnn_outputs1)
            rnn_outputs_flat1 = tf.reshape(rnn_outputs_nice1, [-1, state_size1])

            init_op_layer1 = tf.concat(0, [
                tf.log(init_alpha_1),
                tf.log(tf.exp(init_beta_1) - 1.0)
            ])
            op_activation1 = create_layer(rnn_outputs_flat1,
                                          state_size1,
                                          n_output1, name='output_layer1',
                                          activation_function=tf.identity,
                                          init_biases=init_op_layer1
                                          )
    return op_activation1


def training_step_rnn1(logLik_raw1, early_stop_1, lr_1):
    with tf.name_scope("training1"):
        cost1 = -tf.reduce_mean(logLik_raw1[:early_stop_1, :])
        step1 = tf.train.RMSPropOptimizer(learning_rate=lr_1, epsilon=1e-5).minimize(cost1)
    return cost1, step1


with tf.name_scope("weibull_lstm"):
    op_activation1 = simple_lstm1(X_1, early_stop_1, batch_size_1,
                                  name="RNN_weibull_commission1")
    op_activation1 = tf.reshape(op_activation1, [max_seq_len1, batch_size_1, 2])

    a_1, b_1 = tf.split(split_dim=2, num_split=2, value=op_activation1)

    a_1 = tf.exp(a_1[:, :, 0])
    b_1 = tf.nn.softplus(b_1[:, :, 0])

    output_layer_w1 = tf.pack([a_1, b_1], axis=2)
    log_likelihood1 = weibull_logLik_discrete(a_1, b_1, Y_1, U_1, name="logLikelihood1")

    cost_w1, step_w1 = training_step_rnn(log_likelihood1, early_stop_1, lr_1)

    log_likelihood_regularized1 = log_likelihood1 - weibull_beta_penalty(b_1, location=8.0, growth=10.0,
                                                                         name='penalty_calculation1')
    cost_w_pen1, pen_step_w1 = training_step_rnn1(log_likelihood_regularized1, early_stop_1, lr_1)

sess1 = tf.InteractiveSession()
saver1 = tf.train.Saver()

print("second model initialized")

""" init read methods """


def read_from_file_prices(filename_, delimiter_):
    f = open(filename_, "r")
    data = []
    for line in f:
        data.append(float(line.split(delimiter_)[5].replace(",", ".")))
    return data


def read_from_file(filename_, delimiter_):
    f = open(filename_, "r")
    data = []
    for line in f:
        data.append(float(line.split(delimiter_)[5].replace(",", ".")))
    events_data_ = []
    for i in range(0, len(data) - 1):
        events_data_.append(data[i] < data[i + 1])
    return events_data_

""" init get_countdown methods """


def get_countdown_data():
    global dataset
    events_data = read_from_file("TAER_01.csv", ";")[0:24]

    true_time_to_event_data = []
    for i in range(len(events_data)):
        try:
            true_time_to_event_data.append(events_data[i:].index(True))
        except ValueError:
            true_time_to_event_data.append(len(events_data) - i - 1)

    events = np.array([events_data[i:len(events_data) - (5 - 1) + i] for i in range(0, 5)])
    true_time_to_event = np.array(
        [true_time_to_event_data[i:len(true_time_to_event_data) - (5 - 1) + i] for i in range(0, 5)])

    was_event = []
    for i in range(len(events)):
        buff = []
        for j in range(len(events[0])):
            buff.append(0.5 if j == 0 else (1 if events[i][j - 1] else 0))
        was_event.append(buff)
        buff = []
    was_event = np.array(was_event)

    is_censored = (events[:, ::-1].cumsum(1)[:, ::-1] == 0) * 1
    censored_tte = is_censored[:, ::-1].cumsum(1)[:, ::-1] * is_censored + (1 - is_censored) * true_time_to_event
    events = np.copy(events.T * 1.0)
    true_time_to_event = np.copy(true_time_to_event.T * 1.0)
    censored_tte = np.copy(censored_tte.T * 1.0)
    is_censored = np.copy(is_censored.T * 1.0)
    was_event = np.copy(was_event.T * 1.0)
    return censored_tte, events, true_time_to_event, censored_tte, was_event, is_censored


def get_countdown_data1():
    events_data__ = read_from_file_prices("TAER_01.csv", ";")[0:24]

    events_data = [False] * (19 + 4)

    for i in range(len(events_data)):
        events_data[i] = abs(events_data__[i + 1] - events_data__[i]) > (events_data__[i] * 0.000177)

    true_time_to_event_data = []
    for i in range(len(events_data)):
        try:
            true_time_to_event_data.append(events_data[i:].index(True))
        except ValueError:
            true_time_to_event_data.append(len(events_data) - i - 1)

    events = np.array([events_data[i:len(events_data) - (5 - 1) + i] for i in range(0, 5)])
    true_time_to_event = np.array(
        [true_time_to_event_data[i:len(true_time_to_event_data) - (5 - 1) + i] for i in range(0, 5)])

    was_event = []
    for i in range(len(events)):
        buff = []
        for j in range(len(events[0])):
            buff.append(0.5 if j == 0 else (1 if events[i][j - 1] else 0))
        was_event.append(buff)
        buff = []
    was_event = np.array(was_event)

    is_censored = (events[:, ::-1].cumsum(1)[:, ::-1] == 0) * 1
    censored_tte = is_censored[:, ::-1].cumsum(1)[:, ::-1] * is_censored + (1 - is_censored) * true_time_to_event
    events = np.copy(events.T * 1.0)
    true_time_to_event = np.copy(true_time_to_event.T * 1.0)
    censored_tte = np.copy(censored_tte.T * 1.0)
    is_censored = np.copy(is_censored.T * 1.0)
    was_event = np.copy(was_event.T * 1.0)
    return censored_tte, events, true_time_to_event, censored_tte, was_event, is_censored


""" init training methods"""
# for first model


def training_step_censored(learning_rate):
    batch_indx = random.sample(range(5), batch_size)
    early_stop = 20

    u[:, :] = 1 - is_censored[:, batch_indx]
    y[:, :] = censored_tte[:, batch_indx]
    x[:, :, 0] = was_event[:, batch_indx]

    _, cost_val_w = sess.run([step_w, cost_w],
                             feed_dict={X_: x,
                                        Y_: y,
                                        U_: u,
                                        lr_: learning_rate,
                                        early_stop_: early_stop,
                                        batch_size_: batch_size
                                        })
    costs_w_train.append(cost_val_w)
    iterations.append(iteration)


def training_step_censored_penalized(learning_rate):
    batch_indx = random.sample(range(5), batch_size)
    early_stop = 20

    u[:, :] = 1 - is_censored[:, batch_indx]
    y[:, :] = censored_tte[:, batch_indx]
    x[:, :, 0] = was_event[:, batch_indx]

    _, cost_val_w = sess.run([pen_step_w, cost_w_pen],
                             feed_dict={X_: x,
                                        Y_: y,
                                        U_: u,
                                        lr_: learning_rate,
                                        early_stop_: early_stop,
                                        batch_size_: batch_size
                                        })
    costs_w_train.append(cost_val_w)
    iterations.append(iteration)
    return _


def evaluate_train(plot_save_indx):
    eval_indx = range(5)
    early_stop = 20

    y = np.zeros([20, len(eval_indx)])
    x = np.zeros([20, len(eval_indx), n_features])
    u = np.ones([20, len(eval_indx)])

    x[:, :, 0] = was_event
    y[:, :] = true_time_to_event

    op_w, cost_val_w = sess.run(
        [output_layer_w, cost_w],
        feed_dict={X_: x,
                   Y_: y,
                   U_: u,
                   early_stop_: early_stop,
                   batch_size_: len(eval_indx)
                   })

    inner_iterations.append(iteration)
    costs_w_test.append(cost_val_w)

    op_w = op_w.reshape(max_seq_len, len(eval_indx), 2)
    saved_op_w.append(op_w[:, plot_save_indx, :])
    return op_w


# ---------------------------
# for second model


def training_step_censored1(learning_rate):
    batch_indx1 = random.sample(range(5), batch_size1)
    early_stop = 19

    u1[:, :] = 1 - is_censored1[:, batch_indx1]
    y1[:, :] = censored_tte1[:, batch_indx1]
    x1[:, :, 0] = was_event1[:, batch_indx1]

    _1, cost_val_w1 = sess1.run([step_w1, cost_w1],
                                feed_dict={X_1: x1,
                                           Y_1: y1,
                                           U_1: u1,
                                           lr_1: learning_rate,
                                           early_stop_1: early_stop,
                                           batch_size_1: batch_size1
                                           })
    costs_w_train1.append(cost_val_w1)
    iterations1.append(iteration1)


def training_step_censored_penalized1(learning_rate):
    batch_indx1 = random.sample(range(5), batch_size1)
    early_stop1 = 19

    u1[:, :] = 1 - is_censored1[:, batch_indx1]
    y1[:, :] = censored_tte1[:, batch_indx1]
    x1[:, :, 0] = was_event1[:, batch_indx1]

    _1, cost_val_w1 = sess1.run([pen_step_w1, cost_w_pen1],
                                feed_dict={X_1: x1,
                                           Y_1: y1,
                                           U_1: u1,
                                           lr_1: learning_rate,
                                           early_stop_1: early_stop1,
                                           batch_size_1: batch_size1
                                           })
    costs_w_train1.append(cost_val_w1)
    iterations1.append(iteration1)
    return _1


def evaluate_train1(plot_save_indx1):
    eval_indx1 = range(5)
    early_stop1 = 19

    y1 = np.zeros([19, len(eval_indx1)])
    x1 = np.zeros([19, len(eval_indx1), n_features1])
    u1 = np.ones([19, len(eval_indx1)])

    x1[:, :, 0] = was_event1
    y1[:, :] = true_time_to_event1

    op_w1, cost_val_w1 = sess1.run(
        [output_layer_w1, cost_w1],
        feed_dict={X_1: x1,
                   Y_1: y1,
                   U_1: u1,
                   early_stop_1: early_stop1,
                   batch_size_1: 5
                   })

    inner_iterations1.append(iteration1)
    costs_w_test1.append(cost_val_w1)

    op_w1 = op_w1.reshape(max_seq_len1, len(eval_indx1), 2)
    saved_op_w1.append(op_w1[:, plot_save_indx1, :])
    return op_w1


""" start sessions """

# for first model

print("starting session for first model...")

censored_tte, events, true_time_to_event, censored_tte, was_event, is_censored = get_countdown_data()
a_baseline_real = -1.0 / np.log(1.0 - 1.0 / np.mean(true_time_to_event + 1.0))
a_baseline = a_baseline_real

costs_w_train = []
costs_w_test = []

iterations = []
inner_iterations = []

saved_op_w = []

random.seed(1)
np.random.seed(1)

tf.set_random_seed(1)
sess.run(tf.global_variables_initializer(), feed_dict={init_alpha_: [a_baseline]})
iteration = 0

global is_censored, censored_tte, was_event, true_time_to_event
global costs_w_train, iterations, iteration
global inner_iterations, costs_w_test, saved_op_w

print("session for first model started")
# ---------------------

# for second model

print("starting session for second model...")

censored_tte1, events1, true_time_to_event1, censored_tte1, was_event1, is_censored1 = get_countdown_data1()
a_baseline_real1 = -1.0 / np.log(1.0 - 1.0 / np.mean(true_time_to_event1 + 1.0))
a_baseline1 = a_baseline_real1

costs_w_train1 = []
costs_w_test1 = []

iterations1 = []
inner_iterations1 = []

saved_op_w1 = []

random.seed(1)
np.random.seed(1)

tf.set_random_seed(1)
sess1.run(tf.global_variables_initializer(), feed_dict={init_alpha_1: [a_baseline1]})
iteration1 = 0

global is_censored1, censored_tte1, was_event1, true_time_to_event1
global costs_w_train1, iterations1, iteration1
global inner_iterations1, costs_w_test1, saved_op_w1

print("session for second model started")
""" learn models """
# for first model

print("first model training...")

plot_save_indx = (5 + 1) / 2
batch_size = 5
plot_every_nth = 50
n_iterations = 2000

y = np.zeros([20, batch_size])
x = np.zeros([20, batch_size, n_features])
u = np.ones([20, batch_size])

for i in range(n_iterations):
    if i % plot_every_nth == 0:
        op_w = evaluate_train(plot_save_indx)
    iteration += 1
    data = training_step_censored_penalized(0.001)
print("first model trained after iteration:", iteration)

global u, y, x
global batch_size
# ------------------------
# for second model

print("second model training...")

plot_save_indx1 = (5 + 1) / 2
batch_size1 = 5
plot_every_nth1 = 50
n_iterations1 = 2000

y1 = np.zeros([19, batch_size1])
x1 = np.zeros([19, batch_size1, n_features1])
u1 = np.ones([19, batch_size1])

for i in range(n_iterations1):
    if i % plot_every_nth1 == 0:
        op_w1 = evaluate_train1(plot_save_indx1)
    iteration1 += 1
    data1 = training_step_censored_penalized1(0.001)

print("second model trained after iteration:", iteration1)

global u1, y1, x1
global batch_size1
""" init addition server methods """

#
# def get_data(from_, length, need):
#     global dataset
#     events_data = np.array([False] * (need + 4))
#
#     events_data_ = dataset[from_:from_ + length]
#     events_data[:length] = np.array(events_data_)
#     events_data = events_data.tolist()
#
#     true_time_to_event_data = []
#     for i in range(len(events_data)):
#         try:
#             true_time_to_event_data.append(events_data[i:].index(True))
#         except ValueError:
#             true_time_to_event_data.append(len(events_data) - i - 1)
#
#     events = np.array([events_data[i:len(events_data) - (5 - 1) + i] for i in range(0, 5)])
#     true_time_to_event = np.array(
#         [true_time_to_event_data[i:len(true_time_to_event_data) - (5 - 1) + i] for i in range(0, 5)])
#
#     was_event = []
#     for i in range(len(events)):
#         buff = []
#         for j in range(len(events[0])):
#             buff.append(0.5 if j == 0 else (1 if events[i][j - 1] else 0))
#         was_event.append(buff)
#         buff = []
#     was_event = np.array(was_event)
#
#     is_censored = (events[:, ::-1].cumsum(1)[:, ::-1] == 0) * 1
#     censored_tte = is_censored[:, ::-1].cumsum(1)[:, ::-1] * is_censored + (1 - is_censored) * true_time_to_event
#     events = np.copy(events.T * 1.0)
#     true_time_to_event = np.copy(true_time_to_event.T * 1.0)
#     censored_tte = np.copy(censored_tte.T * 1.0)
#     is_censored = np.copy(is_censored.T * 1.0)
#     was_event = np.copy(was_event.T * 1.0)
#     return censored_tte, events, true_time_to_event, censored_tte, was_event, is_censored
#
#
# def get_data1(from_, length, need):
#     global dataset
#     events_data = np.array([False] * (need + 4))
#
#     events_data__ = dataset[from_:from_ + length]
#
#     events_data_ = [False] * (len(events_data__) - 1)
#
#     for i in range(len(events_data_)):
#         events_data_[i] = abs(events_data__[i + 1] - events_data__[i]) > events_data__[i] * 0.000177
#
#     events_data[:len(events_data_)] = np.array(events_data_)
#     events_data = events_data.tolist()
#
#     true_time_to_event_data = []
#     for i in range(len(events_data)):
#         try:
#             true_time_to_event_data.append(events_data[i:].index(True))
#         except ValueError:
#             true_time_to_event_data.append(len(events_data) - i - 1)
#
#     events = np.array([events_data[i:len(events_data) - (5 - 1) + i] for i in range(0, 5)])
#     true_time_to_event = np.array(
#         [true_time_to_event_data[i:len(true_time_to_event_data) - (5 - 1) + i] for i in range(0, 5)])
#
#     was_event = []
#     for i in range(len(events)):
#         buff = []
#         for j in range(len(events[0])):
#             buff.append(0.5 if j == 0 else (1 if events[i][j - 1] else 0))
#         was_event.append(buff)
#         buff = []
#     was_event = np.array(was_event)
#
#     is_censored = (events[:, ::-1].cumsum(1)[:, ::-1] == 0) * 1
#     censored_tte = is_censored[:, ::-1].cumsum(1)[:, ::-1] * is_censored + (1 - is_censored) * true_time_to_event
#     events = np.copy(events.T * 1.0)
#     true_time_to_event = np.copy(true_time_to_event.T * 1.0)
#     censored_tte = np.copy(censored_tte.T * 1.0)
#     is_censored = np.copy(is_censored.T * 1.0)
#     was_event = np.copy(was_event.T * 1.0)
#     return censored_tte, events, true_time_to_event, censored_tte, was_event, is_censored
#
#
# def get_price(index):
#     _data_ = read_from_file_prices("TAER_01.csv", ";")
#     return _data_[index]
#
#
# def trade(money, products, price, commission, type_, commission_money):
#     if type_ == "buy":
#         count = (money - ((money // price) * price * commission)) // price
#         money -= (count * price)
#         commission_money -= count * price * commission
#         products += count
#     if type_ == "sell":
#         profit = products * price
#         money += profit
#         commission_money -= profit * commission
#         products = 0
#     return money, products, commission_money
""" init server methods """


# def learn_algorithm(from_, length):
#     global dataset
#     start = time.time()
#     censored_tte, events, true_time_to_event, censored_tte, was_event, is_censored = get_data(from_, length, 20)
#     a_baseline_real = -1.0 / np.log(1.0 - 1.0 / np.mean(true_time_to_event + 1.0))
#     a_baseline = a_baseline_real
#
#     costs_w_train = []
#     costs_w_test = []
#
#     iterations = []
#     inner_iterations = []
#
#     saved_op_w = []
#
#     random.seed(1)
#     np.random.seed(1)
#
#     tf.set_random_seed(1)
#     sess.run(tf.global_variables_initializer(), feed_dict={init_alpha_: [a_baseline]})
#     iteration = 0
#
#     plot_save_indx = (5 + 1) / 2
#     batch_size = 5
#     plot_every_nth = 1
#     n_iterations = 1000
#
#     y = np.zeros([20, batch_size])
#     x = np.zeros([20, batch_size, n_features])
#     u = np.ones([20, batch_size])
#
#     for i in range(n_iterations):
#         if time.time() - start > 0.95:
#             break
#         op_w = evaluate_train(plot_save_indx)
#         data = training_step_censored_penalized(0.001)
#
#
# def learn_algorithm1(from_, length):
#     start = time.time()
#     censored_tte1, events1, true_time_to_event1, censored_tte1, was_event1, is_censored1 = get_data1(from_, length, 19)
#     a_baseline_real1 = -1.0 / np.log(1.0 - 1.0 / np.mean(true_time_to_event1 + 1.0))
#     a_baseline1 = a_baseline_real1
#
#     costs_w_train1 = []
#     costs_w_test1 = []
#
#     iterations1 = []
#     inner_iterations1 = []
#
#     saved_op_w1 = []
#
#     random.seed(1)
#     np.random.seed(1)
#
#     tf.set_random_seed(1)
#     sess1.run(tf.global_variables_initializer(), feed_dict={init_alpha_1: [a_baseline1]})
#     iteration = 0
#
#     plot_save_indx1 = (5 + 1) / 2
#     batch_size1 = 5
#     plot_every_nth1 = 1
#     n_iterations1 = 1000
#
#     y1 = np.zeros([19, batch_size])
#     x1 = np.zeros([19, batch_size, n_features])
#     u1 = np.ones([19, batch_size])
#
#     for i in range(n_iterations1):
#         if time.time() - start > 14.90:
#             break
#         op_w1 = evaluate_train1(plot_save_indx1)
#         data1 = training_step_censored_penalized1(0.001)
#
#
# def send_price(last_price):
#     global dataset
#     dataset.append(last_price)
#
#
# def get_forecast(from_, length):
#     censored_tte, events, true_time_to_event, censored_tte, was_event, is_censored = get_data(from_, length, 20)
#
#     eval_indx = range(5)
#     early_stop = 20
#
#     y = np.zeros([20, len(eval_indx)])
#     x = np.zeros([20, len(eval_indx), n_features])
#     u = np.ones([20, len(eval_indx)])
#
#     x[:, :, 0] = was_event
#     y[:, :] = [[13] * 5] * 20
#
#     op_w, cost_val_w = sess.run(
#         [output_layer_w, cost_w],
#         feed_dict={X_: x,
#                    Y_: y,
#                    U_: u,
#                    early_stop_: early_stop,
#                    batch_size_: len(eval_indx)
#                    })
#
#     op_w = op_w.reshape(max_seq_len, len(eval_indx), 2)
#     saved_op_w.append(op_w[:, plot_save_indx, :])
#
#     alpha_pred, beta_pred = np.copy(saved_op_w[-1][:, 0]), np.copy(saved_op_w[-1][:, 1])
#
#     return round(alpha_pred[-1]) + 1
#
#
# def get_forecast1(from_, length):
#     censored_tte1, events1, true_time_to_event1, censored_tte1, was_event1, is_censored1 = get_data1(from_, length, 19)
#
#     eval_indx1 = range(5)
#     early_stop1 = 19
#
#     y1 = np.zeros([19, len(eval_indx1)])
#     x1 = np.zeros([19, len(eval_indx1), n_features1])
#     u1 = np.ones([19, len(eval_indx1)])
#
#     x1[:, :, 0] = was_event1
#     y1[:, :] = [[13] * 5] * 19
#
#     op_w1, cost_val_w1 = sess1.run(
#         [output_layer_w1, cost_w1],
#         feed_dict={X_1: x1,
#                    Y_1: y1,
#                    U_1: u1,
#                    early_stop_1: early_stop1,
#                    batch_size_1: len(eval_indx1)
#                    })
#
#     op_w1 = op_w1.reshape(max_seq_len1, len(eval_indx1), 2)
#     saved_op_w1.append(op_w1[:, plot_save_indx1, :])
#
#     alpha_pred1, beta_pred1 = np.copy(saved_op_w1[-1][:, 0]), np.copy(saved_op_w1[-1][:, 1])
#
#     return round(alpha_pred1[-1])
#
# true_positive = 0
# true_negative = 0
# false_positive = 0
# false_negative = 0
#
# true_positive1 = 0
# true_negative1 = 0
# false_positive1 = 0
# false_negative1 = 0
#
# acc = 0
#
# positive_delta = []
# negative_delta = []
#
# money = 10000
# commission_money = 100
# products = 0
# commission = 0.000177
# buy = "buy"
# sell = "sell"
#
# last_predict = 0
# iter_from_last_predict = 0
#
# for i in range(25, len(read_from_file_prices("TAER_01.csv", ";"))):
#     price = get_price(i)
#     send_price(price)
#     if i % 50 == 0:
#         learn_algorithm(i-23, 24)
#         learn_algorithm1(i-23, 24)
#     if iter_from_last_predict == last_predict:
#         last_predict = get_forecast(i - 13, 14)
#         iter_from_last_predict = 0
#         money, products, commission_money = trade(money, products, price, commission, buy, commission_money)
#     else:
#         money, products, commission_money = trade(money, products, price, commission, sell, commission_money)
#     print("Money:", money, "Price:", price, "Commission money:", commission_money)
#     print("Predict:", last_predict)
#     iter_from_last_predict += 1


def get_data(from_, length, need):
    events_data = np.array([False] * (need + 4))

    events_data_ = read_from_file("TAER_01.csv", ";")[from_:from_ + length]
    events_data[:length] = np.array(events_data_)
    events_data = events_data.tolist()

    true_time_to_event_data = []
    for i in range(len(events_data)):
        try:
            true_time_to_event_data.append(events_data[i:].index(True))
        except ValueError:
            true_time_to_event_data.append(len(events_data) - i - 1)

    events = np.array([events_data[i:len(events_data) - (5 - 1) + i] for i in range(0, 5)])
    true_time_to_event = np.array(
        [true_time_to_event_data[i:len(true_time_to_event_data) - (5 - 1) + i] for i in range(0, 5)])

    was_event = []
    for i in range(len(events)):
        buff = []
        for j in range(len(events[0])):
            buff.append(0.5 if j == 0 else (1 if events[i][j - 1] else 0))
        was_event.append(buff)
        buff = []
    was_event = np.array(was_event)

    is_censored = (events[:, ::-1].cumsum(1)[:, ::-1] == 0) * 1  # Always works (?)
    censored_tte = is_censored[:, ::-1].cumsum(1)[:, ::-1] * is_censored + (1 - is_censored) * true_time_to_event
    events = np.copy(events.T * 1.0)
    true_time_to_event = np.copy(true_time_to_event.T * 1.0)
    censored_tte = np.copy(censored_tte.T * 1.0)
    is_censored = np.copy(is_censored.T * 1.0)
    was_event = np.copy(was_event.T * 1.0)
    return censored_tte, events, true_time_to_event, censored_tte, was_event, is_censored


def get_data1(from_, length, need):
    events_data = np.array([False] * (need + 4))

    events_data__ = read_from_file_prices("TAER_01.csv", ";")[from_:from_ + length]

    events_data_ = [False] * (len(events_data__) - 1)

    for i in range(len(events_data_)):
        events_data_[i] = abs(events_data__[i + 1] - events_data__[i]) > events_data__[i] * 0.000177

    events_data[:len(events_data_)] = np.array(events_data_)
    events_data = events_data.tolist()

    true_time_to_event_data = []
    for i in range(len(events_data)):
        try:
            true_time_to_event_data.append(events_data[i:].index(True))
        except ValueError:
            true_time_to_event_data.append(len(events_data) - i - 1)

    events = np.array([events_data[i:len(events_data) - (5 - 1) + i] for i in range(0, 5)])
    true_time_to_event = np.array(
        [true_time_to_event_data[i:len(true_time_to_event_data) - (5 - 1) + i] for i in range(0, 5)])

    was_event = []
    for i in range(len(events)):
        buff = []
        for j in range(len(events[0])):
            buff.append(0.5 if j == 0 else (1 if events[i][j - 1] else 0))
        was_event.append(buff)
        buff = []
    was_event = np.array(was_event)

    is_censored = (events[:, ::-1].cumsum(1)[:, ::-1] == 0) * 1  # Always works (?)
    censored_tte = is_censored[:, ::-1].cumsum(1)[:, ::-1] * is_censored + (1 - is_censored) * true_time_to_event
    events = np.copy(events.T * 1.0)
    true_time_to_event = np.copy(true_time_to_event.T * 1.0)
    censored_tte = np.copy(censored_tte.T * 1.0)
    is_censored = np.copy(is_censored.T * 1.0)
    was_event = np.copy(was_event.T * 1.0)
    return censored_tte, events, true_time_to_event, censored_tte, was_event, is_censored


def predict(from_, length):
    censored_tte, events, true_time_to_event, censored_tte, was_event, is_censored = get_data(from_, length, 20)

    eval_indx = range(5)
    early_stop = 20

    y = np.zeros([20, len(eval_indx)])
    x = np.zeros([20, len(eval_indx), n_features])
    u = np.ones([20, len(eval_indx)])

    x[:, :, 0] = was_event
    y[:, :] = [[13] * 5] * 20

    op_w, cost_val_w = sess.run(
        [output_layer_w, cost_w],
        feed_dict={X_: x,
                   Y_: y,
                   U_: u,
                   early_stop_: early_stop,
                   batch_size_: len(eval_indx)
                   })

    op_w = op_w.reshape(max_seq_len, len(eval_indx), 2)
    saved_op_w.append(op_w[:, plot_save_indx, :])

    alpha_pred, beta_pred = np.copy(saved_op_w[-1][:, 0]), np.copy(saved_op_w[-1][:, 1])

    return round(alpha_pred[-1]) + 1


def predict1(from_, length):
    censored_tte1, events1, true_time_to_event1, censored_tte1, was_event1, is_censored1 = get_data1(from_, length, 19)

    eval_indx1 = range(5)
    early_stop1 = 19

    y1 = np.zeros([19, len(eval_indx1)])
    x1 = np.zeros([19, len(eval_indx1), n_features1])
    u1 = np.ones([19, len(eval_indx1)])

    x1[:, :, 0] = was_event1
    y1[:, :] = [[13] * 5] * 19

    op_w1, cost_val_w1 = sess1.run(
        [output_layer_w1, cost_w1],
        feed_dict={X_1: x1,
                   Y_1: y1,
                   U_1: u1,
                   early_stop_1: early_stop1,
                   batch_size_1: len(eval_indx1)
                   })

    op_w1 = op_w1.reshape(max_seq_len1, len(eval_indx1), 2)
    saved_op_w1.append(op_w1[:, plot_save_indx1, :])

    alpha_pred1, beta_pred1 = np.copy(saved_op_w1[-1][:, 0]), np.copy(saved_op_w1[-1][:, 1])

    return round(alpha_pred1[-1])


def get_true_predict(index):
    return read_from_file("TAER_01.csv", ";")[index]


def get_true_predict1(index):
    __data__ = read_from_file_prices("TAER_01.csv", ";")
    return abs(__data__[index + 1] - __data__[index]) > __data__[index] * 0.000177


def learn1(from_, length):
    start = time.time()
    censored_tte1, events1, true_time_to_event1, censored_tte1, was_event1, is_censored1 = get_data1(from_, length, 19)
    a_baseline_real1 = -1.0 / np.log(1.0 - 1.0 / np.mean(true_time_to_event1 + 1.0))
    a_baseline1 = a_baseline_real1

    costs_w_train1 = []
    costs_w_test1 = []

    iterations1 = []
    inner_iterations1 = []

    saved_op_w1 = []

    random.seed(1)
    np.random.seed(1)

    tf.set_random_seed(1)
    sess1.run(tf.global_variables_initializer(), feed_dict={init_alpha_1: [a_baseline1]})
    iteration = 0

    plot_save_indx1 = (5 + 1) / 2
    batch_size1 = 5
    plot_every_nth1 = 1
    n_iterations1 = 1000

    y1 = np.zeros([19, batch_size])
    x1 = np.zeros([19, batch_size, n_features])
    u1 = np.ones([19, batch_size])

    for i in range(n_iterations1):
        if time.time() - start > 14.90:
            break
        op_w1 = evaluate_train1(plot_save_indx1)
        data1 = training_step_censored_penalized1(0.001)


def learn(from_, length):
    start = time.time()
    censored_tte, events, true_time_to_event, censored_tte, was_event, is_censored = get_data(from_, length, 20)
    a_baseline_real = -1.0 / np.log(1.0 - 1.0 / np.mean(true_time_to_event + 1.0))
    a_baseline = a_baseline_real

    costs_w_train = []
    costs_w_test = []

    iterations = []
    inner_iterations = []

    saved_op_w = []

    random.seed(1)
    np.random.seed(1)

    tf.set_random_seed(1)
    sess.run(tf.global_variables_initializer(), feed_dict={init_alpha_: [a_baseline]})
    iteration = 0

    plot_save_indx = (5 + 1) / 2
    batch_size = 5
    plot_every_nth = 1
    n_iterations = 1000

    y = np.zeros([20, batch_size])
    x = np.zeros([20, batch_size, n_features])
    u = np.ones([20, batch_size])

    for i in range(n_iterations):
        if time.time() - start > 0.95:
            break
        op_w = evaluate_train(plot_save_indx)
        data = training_step_censored_penalized(0.001)


def get_delta_between_from_and_next(from_):
    _data_ = read_from_file_prices("TAER_01.csv", ";")
    return _data_[from_ + 1] - _data_[from_]


def get_price(index):
    _data_ = read_from_file_prices("TAER_01.csv", ";")
    return _data_[index]


def trade(money, products, price, commission, type_, commission_money):
    if type_ == "buy":
        count = (money - ((money // price) * price * commission)) // price
        money -= (count * price)
        commission_money -= count * price * commission
        products += count
    if type_ == "sell":
        profit = products * price
        money += profit
        commission_money -= profit * commission
        products = 0
    return money, products, commission_money


dataset = read_from_file("TAER_01.csv", ";")

true_positive = 0
true_negative = 0
false_positive = 0
false_negative = 0

true_positive1 = 0
true_negative1 = 0
false_positive1 = 0
false_negative1 = 0

acc = 0

positive_delta = []
negative_delta = []

money = 10000
commission_money = 100
products = 0
commission = 0.000177
buy = "buy"
sell = "sell"

last_predict = 0
iter_from_last_predict = 0
for i in range(24, len(dataset)):
    if i % 23 == 0:
        learn(i - 23, 24)
        learn1(i - 23, 24)
    # commission_predict = predict1(i - 13, 14) == 0
    commission_predict = True
    print(commission_predict, get_true_predict1(i))
    if commission_predict:
        if get_true_predict1(i):
            true_positive1 += 1
        else:
            false_positive1 += 1
    elif not commission_predict:
        if get_true_predict1(i):
            false_negative1 += 1
        else:
            true_negative1 += 1
    if iter_from_last_predict == last_predict:
        last_predict = predict(i - 13, 14)
        iter_from_last_predict = 0
        if commission_predict:
            money, products, commission_money = trade(money, products, get_price(i), commission, buy, commission_money)
        if get_true_predict(i) != True:
            false_positive += 1
            negative_delta.append(abs(get_delta_between_from_and_next(i)))
        else:
            true_positive += 1
            positive_delta.append(abs(get_delta_between_from_and_next(i)))
    else:
        if commission_predict:
            money, products, commission_money = trade(money, products, get_price(i), commission, sell, commission_money)
        if get_true_predict(i) != False:
            false_negative += 1
            negative_delta.append(abs(get_delta_between_from_and_next(i)))
        else:
            true_negative += 1
            positive_delta.append(abs(get_delta_between_from_and_next(i)))
    if i % 1 == 0:
        confusion_matrix = [[true_positive, false_negative],
                            [false_positive, true_negative]]
        accuracy = (true_positive + true_negative) / (true_positive + false_negative + false_positive + true_negative)
        accuracy1 = (true_positive1 + true_negative1) / (
        true_positive1 + false_negative1 + false_positive1 + true_negative1)
        try:
            sensitivity = true_positive / (true_positive + false_negative)
        except ZeroDivisionError:
            sensitivity = 0
        try:
            precision = true_positive / (true_positive + false_positive)
        except ZeroDivisionError:
            precision = 0
        print("Accuracy:", accuracy, "Sensitivity:", sensitivity, "Precision:", precision)
        # print("Accuracy commission:", accuracy1)
        print("Positive delta sum:", sum(positive_delta), "Negative delta sum:", sum(negative_delta))
        print("Money:", money, "Products:", products, "Commission money:", commission_money)
        acc = accuracy
    iter_from_last_predict += 1
